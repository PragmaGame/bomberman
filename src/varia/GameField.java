package varia;

import data.*;
import monster.AI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class GameField extends JPanel implements ActionListener
{
    private Timer timer;
    private Player player;
    private boolean isGame = false;
    private boolean isGameOver = false;
    private boolean isResumeGame = false;
    private boolean isGameMenu = true;
    private boolean isGameMenuRecord = false;
    private boolean isGameMenuAbout = false;
    private boolean isSaveRecord = false;
    private final byte width = 29, height = 15;
    private AI ai;
    private Block[][] field = new Block[height][width];
    private PanelMenu panelMenu = new PanelMenu();
    private PanelMenuButtonBack panelMenuButtonBack = new PanelMenuButtonBack();
    private PanelMenuButtonResume panelMenuButtonResume = new PanelMenuButtonResume();
    private DataBase dataBase = new DataBase();
    private ArrayList<RecordScore> listRecordScore = dataBase.loadingRecord();
    private SerializationFile serFile = new SerializationFile();


    public boolean[] getBooleanVaria()
    {
        boolean[] b = {isGame,isGameOver,isResumeGame,isGameMenu,isGameMenuRecord,isGameMenuAbout};
        return b;
    }

    private void initField()
    {
        for (int i = 0; i < height ; i++)
            for (int j = 0; j < width;j++)
            {
                field[i][j] = new Block();
            }
    }
    private void loadField(SerializationFile file)
    {
        try
        {
            field = file._block;
            player = file._player;
            ai = file._ai;
            isGame = file._bool[0];
            isGameOver = file._bool[1];
            isResumeGame = file._bool[2];
            isGameMenu = file._bool[3];
            isGameMenuRecord = file._bool[4];
            isGameMenuAbout = file._bool[5];
            timer = new Timer(125,this);
        }
        catch (Exception ex)
        {
            System.out.println("error in GameField.loadField");
            initField();
        }
    }

    static FieldKeyListener keyboard;
    private FieldKeyListener getInstance()
    {
        if (keyboard == null)
        {
            keyboard = new FieldKeyListener();
        }
        return keyboard;
    }

    private void newGame()
    {
        isSaveRecord = false;
        isGameOver = false;
        isGameMenu = false;
        isGame = true;
        keyboard.setGame(true);
        timer = new Timer(125,this);
        timer.start();
        player = new Player();
        ai = new AI();
        Levels.loadLevel(field,1);
    }

    public GameField(SerializationFile file)
    {
        setFocusable(true);
        addKeyListener(getInstance());
        LoadIcon.LoadImages();
        if(file == null)
        {
            initField();
        }
        else
        {
            loadField(file);
        }
        this.add(panelMenu);
        this.add(panelMenuButtonBack);
        this.add(panelMenuButtonResume);
    }



    private void serialization()
    {
        serFile.SaveStateGame(player,field,getBooleanVaria(),ai);
        Serialization.SerializableGame(serFile);
    }

    private void pause()
    {

    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(isGame && keyboard.getGame())
        {
            pause();
            player.move(field);
            player.attack(ai,field);
            ai.run(field,player);
            isGame = (player.gameOver() && !ai.Victory());
            isGameOver = !isGame;
        }
        else
        {
            if(!keyboard.getGame())
            {
                isResumeGame = true;
                keyboard.setGame(true);
            }
            System.out.println("Menu");
            timer.stop();
            isGameMenu = true;
            isGame = false;
        }
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        if(keyboard.getDevelopPanel())
        {
            System.out.println("Panel");
            PanelDevelop panelDevelop = new PanelDevelop();
            panelDevelop.setVisible(true);
            keyboard.setDevelopPanel(false);
        }
        if(isGameOver && !isSaveRecord)
        {
            System.out.println("SaveNameRecord");
            PanelRecord panelRecord = new PanelRecord();
            panelRecord.setVisible(true);
            isSaveRecord = true;
        }
        if(isGameMenu)
        {
            g.drawImage(LoadIcon.menuBackground, 0, 0, null);
            panelMenu.setLocation(750,200);
            panelMenu.setVisible(true);
            panelMenuButtonBack.setVisible(false);
            if(isResumeGame)
            {
                panelMenuButtonResume.setLocation(750,140);
                panelMenuButtonResume.setVisible(true);
            }
            if(isGameMenuRecord)
            {
                printGameMenuRecord(g);
                panelMenuButtonResume.setVisible(false);
            }
            if(isGameMenuAbout)
            {
                panelMenuButtonResume.setVisible(false);
                printGameMenuAbout(g);
            }
        }
        else
        {
            panelMenuButtonResume.setVisible(false);
            panelMenu.setVisible(false);
            g.drawImage(LoadIcon.background, 0, 0, null);
            showStatistics(g);
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++) {
                    Image tempImage = null;
                    switch (field[i][j].getObject()) {
                    /*  0 = Пусто
                        1 = Игрок
                        2 = Противник Призрак
                        3 = Противник Скилет
                        4 = Бомба
                        5 = Препятствие
                        6 = Нерушимое припятствие
                        7 = Броня
                        8 = Здоровье
                        9 = Взрыв
                        33 = Рыцарь со щитом
                        10 = Паук
                        11 = Токсичный паук
                        12 = Паутина
                        31 = копье
                    */
                        case NOTHING:
                            tempImage = null;
                            break;
                        case PLAYER:
                            tempImage = LoadIcon.player;
                            break;
                        case ENEMY_GHOST:
                            tempImage = LoadIcon.ghost;
                            break;
                        case ENEMY_KNIGHT:
                            tempImage = LoadIcon.knight;
                            break;
                        case BOMB:
                            tempImage = LoadIcon.bomb;
                            break;
                        case FENCE:
                            tempImage = LoadIcon.fence;
                            break;
                        case FENCE_IROCLAND:
                            tempImage = LoadIcon.fenceironclad;
                            break;
                        case SHIELD:
                            tempImage = LoadIcon.shield;
                            break;
                        case HELTH:
                            tempImage = LoadIcon.health;
                            break;
                        case EXPLOSION:
                            tempImage = LoadIcon.explosion;
                            break;
                        case ENEMY_SPIDER:
                            tempImage = LoadIcon.spider;
                            break;
                        case ENEMY_KNIGHT_SHIELD:
                            tempImage = LoadIcon.knightshield;
                            break;
                        case ENEMY_SPISER_TOXIC:
                            tempImage = LoadIcon.spidertoxic;
                            break;
                        case WEB:
                            tempImage = LoadIcon.web;
                            break;
                        case BUF_BOMB:
                            tempImage = LoadIcon.bufBomb;
                            break;
                        case BOSS:
                            tempImage = LoadIcon.Boss;
                            break;
                        case BUF_BOMB_DAMAGE:
                            tempImage = LoadIcon.bufBombDamage;
                            break;
                        case BUF_BOMB_RADIUS:
                            tempImage = LoadIcon.bufBombRadius;
                            break;
                        case SPEAR:
                            tempImage = LoadIcon.Spear;
                            break;
                        default:
                            System.out.println("Ошибка отрисовки. switch case");
                            break;
                    }
                    g.drawImage(tempImage, field[i][j].getxPos(), field[i][j].getyPos(), null);
                }
            showExplosion();
        }
    }

    class PanelMenuButtonResume extends JPanel
    {
        PanelMenuButtonResume()
        {
            JButton buttonResume = new JButton();
            this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
            buttonResume.setAlignmentX(JComponent.CENTER_ALIGNMENT);
            buttonResume.setPreferredSize(new Dimension(290, 64));
            buttonResume.setIcon(new ImageIcon("Icon/buttonResume.png"));
            buttonResume.addActionListener(new ResumeActionListener());
            this.setVisible(false);
            this.add(buttonResume);
        }
    }

    class ResumeActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            isResumeGame = false;
            timer.start();
            isGameMenu = false;
            isGame = true;
            isGameOver = false;
            panelMenuButtonResume.setVisible(true);
            repaint();
        }
    }



    class PanelRecord extends JFrame
    {
        public PanelRecord()
        {
            setTitle("SAVE RECORD");
            setResizable(false);
            setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            // 1856 960
            setSize(400,270);
            setLocation(0,0);
            setVisible(false);

            JLabel label = new JLabel();
            label.setText("Game Over");
            label.setFont(new Font("Verdana", Font.PLAIN, 64));
            label.setPreferredSize(new Dimension(400, 80));


            JLabel label1 = new JLabel();
            label1.setText("<html>Введите ваш ник для<br>сохранения рекорда: " + player.getScore()+"</html>");
            label1.setFont(new Font("Verdana", Font.PLAIN, 24));
            label1.setPreferredSize(new Dimension(400, 150));

            JTextField textField = new JTextField();
            textField.setPreferredSize(new Dimension(400, 40));

            JPanel p = new JPanel();
            p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
            p.setBackground(Color.GREEN);


            p.add(label);
            p.add(label1);
            p.add(textField);
            this.add(p);

            textField.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    //RecordScore.saveRecord(listRecordScore,player.getScore(),textField.getText());
                    System.out.println(textField.getText());
                    textField.removeActionListener(this);
                    textField.setText("Рекорд Сохранен");
                }
            });
        }
    }

    class PanelDevelop extends JFrame
    {
        public PanelDevelop()
        {
            setTitle("DEVELOP");
            setResizable(false);
            setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            // 1856 960
            setSize(400,100);
            setLocation(0,0);
            setVisible(false);

            JTextField textField = new JTextField(1);
            this.add(textField);

            textField.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e)
                {
                    int flag = commandDevelop(textField.getText());
                    String str;
                    switch (flag)
                    {
                        case 0: str = "Completed command\n";break;
                        case 1: str = "No this command. Enter help\n";break;
                        case 2: str = "<param1> <param2> <param3>\n <param1> : help | god | change\n<param2> and <param3> if <param1> = change\n <param2> = shield | health | amountBomb | damageBomb | radiusBomb\n <param3> = int\n\nExample : god\nExample : change health 100";break;
                        default: str = "default";
                    }
                    // Отображение введенного текста
                    JOptionPane.showMessageDialog(PanelDevelop.this, str);
                    textField.setText("");
                }
            });
        }

        private int commandDevelop(String commandDev)
        {
            int flagErr = 0;
            commandDev = commandDev.trim();
            commandDev = commandDev.toLowerCase();
            String[] words = commandDev.split(" ");

            switch (words[0])
            {
                case "help": flagErr = 2; break;
                case "dead": player.changeParam("health",0);
                case "kill_all": ai.removeAllMonsters();break;
                case "god":
                {
                    player.changeParam("shield",999);
                    player.changeParam("health",999);
                    player.changeParam("amountBomb",10);
                    player.changeParam("damageBomb",1000);
                    player.changeParam("radiusBomb",10);
                    break;
                }
                case "change":player.changeParam(words[1],Integer.parseInt(words[2])); break;
                default: flagErr = 1;
            }

            return flagErr;
        }

    }


    class PanelMenuButtonBack extends JPanel
    {
        PanelMenuButtonBack()
        {
            JButton buttonBack = new JButton();
            this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
            buttonBack.setAlignmentX(JComponent.CENTER_ALIGNMENT);
            buttonBack.setPreferredSize(new Dimension(290, 64));
            buttonBack.setIcon(new ImageIcon("Icon/buttonBack.png"));
            buttonBack.addActionListener(new BackActionListener());
            this.setVisible(false);
            this.add(buttonBack);
        }
    }

    class BackActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            System.out.println("ListenerBack");
            if(isGameMenuRecord)
            {
                isGameMenuRecord = false;
            }
            if (isGameMenuAbout)
            {
                isGameMenuAbout = false;
            }
            repaint();
        }
    }

    class PanelMenu extends JPanel
    {
        PanelMenu()
        {
            JButton buttonNewGame = new JButton();
            JButton buttonRecord = new JButton();
            JButton buttonExit = new JButton();
            JButton buttonAbout = new JButton();
            this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
            this.setPreferredSize(new Dimension(290,264));

            initButton(buttonNewGame);
            initButton(buttonRecord);
            initButton(buttonAbout);
            initButton(buttonExit);

            buttonNewGame.addActionListener(new NewGameActionListener());
            buttonRecord.addActionListener(new RecordActionListener());
            buttonAbout.addActionListener(new AboutActionListener());
            buttonExit.addActionListener(new ExitActionListener());


            buttonNewGame.setIcon(new ImageIcon("Icon/buttonNewGame.png"));
            buttonRecord.setIcon(new ImageIcon("Icon/buttonRecord.png"));
            buttonAbout.setIcon(new ImageIcon("Icon/buttonAbout.png"));
            buttonExit.setIcon(new ImageIcon("Icon/buttonExit.png"));

            this.add(buttonNewGame);
            this.add(buttonRecord);
            this.add(buttonAbout);
            this.add(buttonExit);
        }

        private void initButton(JButton button)
        {
            button.setAlignmentX(JComponent.CENTER_ALIGNMENT);
            button.setPreferredSize(new Dimension(290, 64));
            //button.setBorderPainted(false);
        }
    }

    class NewGameActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            System.out.println("ListenerNewGame");
            newGame();
        }
    }

    class RecordActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            System.out.println("ListenerRecord");
            isGameMenuRecord = true;
            repaint();
        }
    }

    class AboutActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            System.out.println("ListenerAbout");
            isGameMenuAbout = true;
            repaint();
        }
    }

    class ExitActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            dataBase.saveRecord(listRecordScore);
            System.out.println("ListenerExit");
            if (isResumeGame)
            {
                serialization();
            }
            System.exit(0);
        }
    }

    private void showStatistics(Graphics g)
    {
        final int widthStatistics = 40;
        final int heightStatistics = 12;
        g.setColor(Color.darkGray);
        g.fillRect(0,0,1872,50);
        g.setColor(Color.WHITE);
        Font currentFont = g.getFont();
        Font newFont = currentFont.deriveFont(currentFont.getSize() * 2.5F);
        g.setFont(newFont);
        g.drawImage(LoadIcon.HealthStatistics, 100,heightStatistics, null);
        g.drawString(" : "+ player.getHealth(),132,widthStatistics);
        g.drawImage(LoadIcon.ShieldStatistics, 250,heightStatistics, null);
        g.drawString(" : "+ player.getShield(),280,widthStatistics);
        g.drawImage(LoadIcon.ScoreStatistics, 400,heightStatistics, null);
        g.drawString(" : "+ player.getScore(),430,widthStatistics);
        g.drawImage(LoadIcon.BombStatistics, 560,heightStatistics, null);
        g.drawString(" : "+ player.getAmountBomb(),600,widthStatistics);
    }

    private void showExplosion()
    {
            for(int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    if (field[i][j].getObject() == ObjectField.EXPLOSION || field[i][j].getObject() == ObjectField.SPEAR)
                    {
                        field[i][j].setObject(ObjectField.NOTHING);
                    }
                }
    }

    private void printGameMenuRecord(Graphics g)
    {
        panelMenu.setVisible(false);

        panelMenuButtonBack.setVisible(true);
        panelMenuButtonBack.setLocation(750,700);

        Font currentFont = g.getFont();
        Font newFont = currentFont.deriveFont(currentFont.getSize() * 3F);
        g.setFont(newFont);

        int height = 200;
        for (int i = 0 ; i < listRecordScore.size(); i++)
        {
            int width = 500;
            g.drawString((i + 1)  + ".",width,height);
            g.drawString("Score : " + listRecordScore.get(i).gameRecordScore , width += 50, height);
            g.drawString("Date  : " + listRecordScore.get(i).dateRecord, width += 200, height);
            g.drawString("Name  : " + listRecordScore.get(i).userName, width += 330, height);
            height += 50;
        }
    }

    private void printGameMenuAbout(Graphics g)
    {
        panelMenu.setVisible(false);

        panelMenuButtonBack.setVisible(true);
        panelMenuButtonBack.setLocation(750,700);

        Font currentFont = g.getFont();
        Font newFont = currentFont.deriveFont(currentFont.getSize() * 3F);
        g.setFont(newFont);

        g.drawString("Created by Kurda Mark ", 750, 100);
        g.drawString("       © Copyright",750,150);
        g.drawString("L - Pause", 500, 250);
        g.drawString("UP - Move up", 500, 300);
        g.drawString("DOWN - Move down", 500, 350);
        g.drawString("LEFT - Move left", 500, 400);
        g.drawString("RIGHT - Move right", 500, 450);
        g.drawString("Игрок побеждает убив всех монстров.", 550, 500);
        g.drawString("Очки начисляются за убийство монстров,", 550, 550);
        g.drawString("и снижаются за лишнии действия", 550, 600);
        g.drawString("подбор перков, нанесение урона самому себе", 550, 650);
    }
}
