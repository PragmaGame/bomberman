package varia;

import java.io.Serializable;

public class Block implements Serializable
{
    private int xPos;
    private int yPos;
    /*  0 = Пусто
        1 = Игрок
        2 = Противник Призрак
        3 = Противник Скилет
        4 = Бомба
        5 = Препятствие
        6 = Нерушимое припятствие
        7 = Броня
        8 = Здоровье
        9 = Взрыв
        33 = Рыцарь со щитом
        10 = Паук
        11 = Токсичный паук
        12 = Паутина
        31 = копье
     */
    private ObjectField objectField;
    private int ID;

    Block()
    {
        ID = 0;
        objectField = null;
        xPos = 0;
        yPos = 0;
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public ObjectField getObject() {
        return objectField;
    }

    public void setObject(ObjectField object) {
        this.objectField = object;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}
