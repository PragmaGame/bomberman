package varia;

public enum ObjectField
{
    /*  0 = Пусто
        1 = Игрок
        2 = Противник Призрак
        3 = Противник Скилет
        4 = Бомба
        5 = Препятствие
        6 = Нерушимое припятствие
        7 = Броня
        8 = Здоровье
        9 = Взрыв
        33 = Рыцарь со щитом
        10 = Паук
        11 = Токсичный паук
        12 = Паутина
        31 = копье
     */

    NOTHING,
    PLAYER,
    ENEMY_GHOST,
    ENEMY_KNIGHT,
    BOMB,
    FENCE,
    FENCE_IROCLAND,
    SHIELD,
    HELTH,
    EXPLOSION,
    ENEMY_KNIGHT_SHIELD,
    ENEMY_SPIDER,
    ENEMY_SPISER_TOXIC,
    WEB,
    SPEAR,
    BOSS,
    BUF_BOMB,
    BUF_BOMB_RADIUS,
    BUF_BOMB_DAMAGE
}
