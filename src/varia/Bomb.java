package varia;

import java.io.Serializable;

public class Bomb implements Serializable
{
    private int yPosBomb;
    private int xPosBomb;
    private int radius;
    private int damage;

    private boolean activation = false;
    private int timerBomb = 10;


    public int getyPosBomb() { return yPosBomb; }
    public int getxPosBomb() { return xPosBomb; }
    public int getRadius() { return radius; }
    public int getDamage() { return damage; }

    public boolean isActivation() { return activation; }
    public void setActivation(boolean activation) { this.activation = activation; }
    public int getTimerBomb() { return timerBomb; }
    public void setTimerBomb(int timerBomb) { this.timerBomb = timerBomb; }


    public Bomb(int yPosBomb,int xPosBomb,int radius,int damage)
    {
        this.yPosBomb = yPosBomb;
        this.xPosBomb = xPosBomb;
        this.radius = radius;
        this.damage = damage;
    }
}
