package varia;

import monster.AI;
import varia.Block;
import varia.Bomb;
import varia.GameField;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Random;

public class Player implements Serializable
{
    private int shield;
    private int health;
    private int live;
    private int score;

    private int xPosPlayer, yPosPlayer;

    private ArrayDeque<Bomb> listBomb = new ArrayDeque<>();
    private int radiusBomb = 3;
    private int damageBomb = 100;
    private int amountBomb = 1;


    public int getShield() { return shield; }
    public int getHealth() { return health; }
    public int getLive() { return live; }
    public int getScore() { return score; }
    public int getAmountBomb() {return amountBomb; }
    public int getDamageBomb() {return damageBomb;}

    public void addScore(int score) {this.score += score;}

    Player()
    {
        xPosPlayer = 1;
        yPosPlayer = 7;
        shield = 100;
        health = 100;
        live = 0;
        score = 0;
    }

    /*  0 = Пусто
        1 = Игрок
        2 = Противник Призрак
        3 = Противник Рыцарь
        4 = Бомба
        5 = Препятствие
        6 = Нерушимое припятствие
        7 = Броня
        8 = Здоровье
        9 = Взрыв
        33 = Рыцарь со щитом
        10 = Паук
        11 = Токсичный паук
        12 = Паутина
        31 = копье
     */

    public void changeParam(String param,int value)
    {
        switch (param)
        {
            case "health": health = value; break;
            case "shield": shield = value; break;
            case "score": score = value; break;
            case "radiusBomb": radiusBomb = value; break;
            case "amountBomb": amountBomb = value; break;
            case "damageBomb": damageBomb = value; break;
        }
    }

    private int moveDirection()
    {
        if(GameField.keyboard.getUp()) { return 1; }
        if(GameField.keyboard.getDown()) { return 2; }
        if (GameField.keyboard.getRight()) { return 3; }
        if (GameField.keyboard.getLeft()) { return 4; }
        return 404;
    }

    public void move(Block[][] FIELD)
    {
        int direction = moveDirection();
        if(direction != 404)
        {
            ObjectField objectField = null;
            switch (direction)
            {
                case 1: objectField = FIELD[yPosPlayer - 1][xPosPlayer].getObject(); break;
                case 2: objectField = FIELD[yPosPlayer + 1][xPosPlayer].getObject(); break;
                case 3: objectField = FIELD[yPosPlayer][xPosPlayer + 1].getObject(); break;
                case 4: objectField = FIELD[yPosPlayer][xPosPlayer - 1].getObject(); break;
            }
            if(moveCheck(objectField))
            {
                checkImprovement(objectField);
                run(FIELD,direction);
                moveFALSE();
            }
            else
            {
                moveFALSE();
            }
        }
    }

    private boolean moveCheck(ObjectField objectField)
    {
        if(objectField == ObjectField.ENEMY_GHOST || objectField == ObjectField.ENEMY_KNIGHT || objectField == ObjectField.ENEMY_KNIGHT_SHIELD || objectField == ObjectField.ENEMY_SPIDER|| objectField == ObjectField.BOSS || objectField == ObjectField.BOMB || objectField == ObjectField.FENCE || objectField == ObjectField.FENCE_IROCLAND )
        {
            return false;
        }
        else
            return true;
    }

    private void moveFALSE()
    {
        GameField.keyboard.setDown(false);
        GameField.keyboard.setUp(false);
        GameField.keyboard.setLeft(false);
        GameField.keyboard.setRight(false);
    }

    private void run(Block[][] FIELD,int direction)
    {
        FIELD[yPosPlayer][xPosPlayer].setObject(ObjectField.NOTHING);
        switch (direction)
        {
            case 1: yPosPlayer--;break;
            case 2: yPosPlayer++;break;
            case 3: xPosPlayer++;break;
            case 4: xPosPlayer--;break;
        }
        FIELD[yPosPlayer][xPosPlayer].setObject(ObjectField.PLAYER);
    }

    private void checkImprovement(ObjectField objectField)
    {
        if(objectField == ObjectField.SHIELD)
        {
            shield += 50;
            score -= 25;
        }
        if(objectField == objectField.HELTH)
        {
            health += 75;
            score -= 35;
        }
        if(objectField == objectField.BUF_BOMB)
        {
            amountBomb++;
            score -= 10;
        }
        if(objectField == ObjectField.WEB)
        {
            damage(10);
            score -= 10;
        }
        if(objectField == ObjectField.BUF_BOMB_DAMAGE)
        {
            damageBomb += 20;
            score -= 5;
        }
        if(objectField == ObjectField.BUF_BOMB_RADIUS)
        {
            radiusBomb++;
            score -= 5;
        }
    }


    public void attack(AI ai, Block[][] FIELD)
    {
        if(GameField.keyboard.getBomb() && amountBomb > listBomb.size())
        {
            GameField.keyboard.setBomb(false);
            Bomb bomb = new Bomb(yPosPlayer,xPosPlayer, radiusBomb, damageBomb);
            listBomb.addLast(bomb);
        }
        else
        {
            GameField.keyboard.setBomb(false);
        }
        if(!listBomb.isEmpty())
        {
            if(listBomb.getLast().isActivation() == false)
            {
                if(yPosPlayer != listBomb.getLast().getyPosBomb() || xPosPlayer != listBomb.getLast().getxPosBomb())
                {
                    FIELD[listBomb.getLast().getyPosBomb()][listBomb.getLast().getxPosBomb()].setObject(ObjectField.BOMB);
                    listBomb.getLast().setActivation(true);
                }
            }
            for (Bomb a : listBomb)
            {
                if (a.isActivation())
                {
                    if(a.getTimerBomb() == 0)
                    {
                        explosion(ai,FIELD,a.getRadius(),a.getDamage(),a.getyPosBomb(),a.getxPosBomb());
                        listBomb.removeFirst();
                        if(listBomb.isEmpty())
                        {
                            break;
                        }
                    }
                    else
                    {
                        a.setTimerBomb(a.getTimerBomb() - 1);
                    }
                }
            }
        }
    }

    private void explosion(AI ai,Block[][] FIELD,int radius,int damage,int yPosBomb,int xPosBomb)
    {
        FIELD[yPosBomb][xPosBomb].setObject(ObjectField.EXPLOSION);
        for(int i = 1 ; i < radius; i++)
        {
            if(explosionField(ai,yPosBomb - i,xPosBomb,FIELD,damage) == false) break;
        }
        for(int i = 1 ; i < radius; i++)
        {
            if(explosionField(ai,yPosBomb + i,xPosBomb,FIELD,damage) == false) break;
        }
        for(int i = 1 ; i < radius; i++)
        {
            if(explosionField(ai,yPosBomb,xPosBomb - i,FIELD,damage) == false) break;
        }
        for(int i = 1 ; i < radius; i++)
        {
            if(explosionField(ai,yPosBomb,xPosBomb + i,FIELD,damage) == false) break;
        }
    }

    private boolean explosionField(AI ai,int yPos, int xPos,Block[][] FIELD,int damage)
    {
        ObjectField objectField = FIELD[yPos][xPos].getObject();
        if(objectField == ObjectField.NOTHING)
        {
            FIELD[yPos][xPos].setObject(ObjectField.EXPLOSION);
        }
        if(objectField == ObjectField.PLAYER)
        {
            damage(damage);
            score -= 20;
            return false;
        }
        if(objectField == ObjectField.SHIELD || objectField == ObjectField.HELTH)
        {
            FIELD[yPos][xPos].setObject(ObjectField.EXPLOSION);
            score-=5;
        }
        if(objectField == ObjectField.WEB)
        {
            FIELD[yPos][xPos].setObject(ObjectField.EXPLOSION);
            score+=10;
        }
        if(objectField == ObjectField.FENCE_IROCLAND || objectField == ObjectField.BOMB)
        {
            return false;
        }
        if(objectField == ObjectField.FENCE)
        {
            FIELD[yPos][xPos].setObject(ObjectField.EXPLOSION);
            score--;
            int random = random();
            if (random == 5)
            {
                FIELD[yPos][xPos].setObject(ObjectField.BUF_BOMB);
            }
            if (random == 7)
            {
                FIELD[yPos][xPos].setObject(ObjectField.HELTH);
            }
            if (random == 9)
            {
                FIELD[yPos][xPos].setObject(ObjectField.SHIELD);
            }
            if (random == 12)
            {
                FIELD[yPos][xPos].setObject(ObjectField.BUF_BOMB_DAMAGE);
            }
            if (random == 15)
            {
                FIELD[yPos][xPos].setObject(ObjectField.BUF_BOMB_RADIUS);
            }
            return false;
        }
        if(objectField == ObjectField.ENEMY_GHOST || objectField == ObjectField.ENEMY_KNIGHT || objectField == ObjectField.ENEMY_KNIGHT_SHIELD || objectField == ObjectField.ENEMY_SPIDER ||objectField == ObjectField.ENEMY_SPISER_TOXIC || objectField == ObjectField.BOSS)
        {
            int temp = score;
            ai.damageMonster(yPos,xPos, this);
            if(temp != score)
            {
                FIELD[yPos][xPos].setObject(ObjectField.NOTHING);
            }
            return false;
        }
        return true;
    }


    int random()
    {
        Random random = new Random();
        return random.nextInt(20);
    }

    public void damage(int d)
    {
        int damageShield = (int)(d / 100.0 * 70);
        int damageHealth = (int)(d / 100.0 * 30);

        if(shield > 0 )
        {
            shield -= damageShield;
            if(shield < 0)
            {
                health += shield;
                shield = 0;
            }
            else
            {
                health -= damageHealth;
            }
        }
        else
        {
            health-= (damageHealth + damageShield);
        }
        if(health <= 0 )
            health = 0;
    }


    public boolean gameOver()
    {
        if (health == 0)
        {
            return false;
        }
        else return true;
    }

}
