package varia;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

class FieldKeyListener extends KeyAdapter
{
    public boolean getRight() {
        return isRight;
    }

    public void setRight(boolean right) {
        isRight = right;
    }

    public boolean getLeft() {
        return isLeft;
    }

    public void setLeft(boolean left) {
        isLeft = left;
    }

    public boolean getUp() {
        return isUp;
    }

    public void setUp(boolean up) {
        isUp = up;
    }

    public boolean getDown() {
        return isDown;
    }

    public void setDown(boolean down) {
        isDown = down;
    }

    public boolean getBomb() {
        return isBomb;
    }

    public void setBomb(boolean bomb) {
        isBomb = bomb;
    }

    public boolean getPause() {
        return isPause;
    }

    public void setPause(boolean pause) {
        isPause = pause;
    }

    public boolean getGame() {return  isGame;}

    public void setGame(boolean game) {isGame = game;}

    public boolean getDevelopPanel() {
        return isDevelopPanel;
    }

    public void setDevelopPanel(boolean developPanel) {
        isDevelopPanel = developPanel;
    }

    private boolean isGame = true;
    private boolean isRight = false;
    private boolean isLeft = false;
    private boolean isUp = false;
    private boolean isDown = false;
    private boolean isBomb = false;
    private boolean isPause = false;
    private boolean isDevelopPanel = false;

    @Override
    public void keyPressed(KeyEvent e)
    {
        super.keyPressed(e);
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_LEFT)
        {
            isLeft = true;
            isUp = false;
            isDown = false;
            isRight = false;
        }
        if(key == KeyEvent.VK_RIGHT)
        {
            isRight = true;
            isUp = false;
            isDown = false;
            isLeft = false;
        }
        if(key == KeyEvent.VK_UP)
        {
            isRight = false;
            isUp = true;
            isLeft = false;
            isDown = false;
        }
        if(key == KeyEvent.VK_DOWN)
        {
            isRight = false;
            isDown = true;
            isLeft = false;
            isUp = false;
        }
        if(key == KeyEvent.VK_SPACE)
        {
            isBomb = true;
        }
        if (key == KeyEvent.VK_ESCAPE)
        {
            isGame = false;
        }
        if(key == KeyEvent.VK_L)
        {

        }
        if(key == KeyEvent.VK_1)
        {
            isDevelopPanel = true;
        }
    }
}