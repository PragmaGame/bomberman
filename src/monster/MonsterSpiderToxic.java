package monster;

import varia.Block;
import varia.ObjectField;
import varia.Player;

public class MonsterSpiderToxic extends MonsterSpider
{
    MonsterSpiderToxic(int y,int x,int id)
    {
        super(y,x,id);
        health = 100;
        damage = 30;
        scoreMonster = 100;
        objectMonster = ObjectField.ENEMY_SPISER_TOXIC;
    }

    int damage(int d)
    {
        int damageShield = d / 100 * 70;
        int damageHealth = d / 100 * 30;

        if(shield > 0 )
        {
            shield -= damageShield;
            if(shield < 0)
            {
                health += shield;
                shield = 0;
            }
            else
            {
                health -= damageHealth;
            }
        }
        else
        {
            health-= (damageHealth + damageShield);
        }
        if(health <= 0 )
            return scoreMonster;
        return 0;
    }

    private void specialAttack(Player player)
    {
        int damageSpecialAttack = 20;
        if(random(5) < 2)
        {
            damagePlayer(player,damageSpecialAttack);
        }
    }

    public boolean findMonster(int y,int x,Player player)
    {
        return y == yPosMonster && x == xPosMonster;
    }

    @Override
    public void AImob(Block[][] FIELD, Player player)
    {
        ObjectField up = FIELD[yPosMonster - 1][xPosMonster].getObject();
        ObjectField down = FIELD[yPosMonster + 1][xPosMonster].getObject();
        ObjectField left = FIELD[yPosMonster][xPosMonster - 1].getObject();
        ObjectField right = FIELD[yPosMonster][xPosMonster + 1].getObject();
        if(up == ObjectField.PLAYER || down == ObjectField.PLAYER || left == ObjectField.PLAYER || right == ObjectField.PLAYER)
        {
            specialAttack(player);
            damagePlayer(player,damage);
        }
        else
        {
            move(FIELD,up,down,left,right);
        }
    }
}
