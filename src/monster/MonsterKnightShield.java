package monster;

import varia.ObjectField;
import varia.Player;

public class MonsterKnightShield extends MonsterKnight
{
    public MonsterKnightShield(int y,int x,int id)
    {
        super(y,x,id);
        health = 100;
        shield = 200;
        damage = 75;
        scoreMonster = 300;
        objectMonster = ObjectField.ENEMY_KNIGHT_SHIELD;
    }


    int damage(int d)
    {
        int damageShield = (int)(d / 100.0 * 90);
        int damageHealth = (int)(d / 100.0 * 10);

        if(shield > 0 )
        {
            shield -= damageShield;
            if(shield < 0)
            {
                health += shield;
                shield = 0;
            }
            else
            {
                health -= damageHealth;
            }
        }
        else
        {
            health-= (damageHealth + damageShield);
        }
        if(health <= 0 )
            return scoreMonster;
        return 0;
    }

    public boolean findMonster(int y, int x, Player player)
    {
        return y == yPosMonster && x == xPosMonster;
    }
}
