package monster;

import varia.Block;
import varia.ObjectField;
import varia.Player;

public class MonsterKnight extends Monster
{
    int amountSpecialAttack = 1;
    boolean flagSpecialAttack = false;
    int targetYpos = 0;
    int targetXpos = 0;
    int outgoingYpos = 0;
    int outgoingXpos = 0;
    int distance = 0;
    int damageSpecialAttack = 50;
    int directionSpecialAttack = 0;

    MonsterKnight(int y,int x,int id)
    {
        ID = id;
        yPosMonster = y;
        xPosMonster = x;
        health = 100;
        shield = 120;
        damage = 75;
        scoreMonster = 200;
        objectMonster = ObjectField.ENEMY_KNIGHT;
    }

    int deray = 0;

    public boolean findMonster(int y,int x,Player player)
    {
        return y == yPosMonster && x == xPosMonster;
    }

    int damage(int d)
    {
        int damageShield = (int)(d / 100.0 * 70);
        int damageHealth = (int)(d / 100.0 * 30);

        if(shield > 0 )
        {
            shield -= damageShield;
            if(shield < 0)
            {
                health += shield;
                shield = 0;
            }
            else
            {
                health -= damageHealth;
            }
        }
        else
        {
            health-= (damageHealth + damageShield);
        }
        if(health <= 0 )
            return scoreMonster;
        return 0;
    }

    private void specialAttack(Block[][] FIELD)
    {
        int radius = 6;
        boolean flagAttack;
        for (int i = 1; i < 5 ; i++)
        {
            flagAttack = apportunitySpecialAttack(FIELD, i,radius);
            if(flagAttack)
            {
                amountSpecialAttack--;
                flagSpecialAttack = true;
                deray = 4;
                break;
            }
        }
    }

    private boolean apportunitySpecialAttack(Block[][] FIELD,int direction ,int radius)
    {
        int object = 0;
        for (int i = 1; i < radius; i++)
        {
            switch (direction)
            {
                case 1:
                    {
                        targetXpos = FIELD[yPosMonster - i][xPosMonster].getxPos();
                        targetYpos = FIELD[yPosMonster - i][xPosMonster].getyPos();
                        object = checkObject(FIELD[yPosMonster - i][xPosMonster].getObject());
                    }break;
                case 2:
                    {
                        targetXpos = FIELD[yPosMonster + i][xPosMonster].getxPos();
                        targetYpos = FIELD[yPosMonster + i][xPosMonster].getyPos();
                        object = checkObject(FIELD[yPosMonster + i][xPosMonster].getObject());
                    }break;
                case 3:
                    {
                        targetXpos = FIELD[yPosMonster][xPosMonster - i].getxPos();
                        targetYpos = FIELD[yPosMonster][xPosMonster - i].getyPos();
                        object = checkObject(FIELD[yPosMonster][xPosMonster - i].getObject());
                    }break;
                case 4:
                    {
                        targetXpos = FIELD[yPosMonster][xPosMonster + i].getxPos();
                        targetYpos = FIELD[yPosMonster][xPosMonster + i].getyPos();
                        object = checkObject(FIELD[yPosMonster][xPosMonster + i].getObject());
                    }break;
            }
            if( object  == -1)
            {
                break;
            }
            if(object  == 1)
            {
                outgoingYpos = yPosMonster;
                outgoingXpos = xPosMonster;
                distance = i;
                directionSpecialAttack = direction;
                System.out.println(direction);
                return true;
            }
        }
        return false;
    }
    
    private int checkObject(ObjectField object)
    {
        if(object == ObjectField.ENEMY_GHOST || object == ObjectField.ENEMY_KNIGHT || object == ObjectField.BOMB ||object == ObjectField.FENCE ||object == ObjectField.FENCE_IROCLAND)
        {
            // Атака не возможна , существует преграда
            return -1;
        }
        if(object == ObjectField.PLAYER)
        {
            // Атака возможна
            return  1;
        }
        if( object == ObjectField.NOTHING || object == ObjectField.HELTH || object == ObjectField.SHIELD )
        {
            // Продолжать проверку
            return 0;
        }
        return 0;
    }


    public void AImob(Block[][] FIELD, Player player)
    {
        if (deray == 0)
        {
            ObjectField up = FIELD[yPosMonster - 1][xPosMonster].getObject();
            ObjectField down = FIELD[yPosMonster + 1][xPosMonster].getObject();
            ObjectField left = FIELD[yPosMonster][xPosMonster - 1].getObject();
            ObjectField right = FIELD[yPosMonster][xPosMonster + 1].getObject();
            if(up == ObjectField.PLAYER || down == ObjectField.PLAYER || left == ObjectField.PLAYER || right == ObjectField.PLAYER)
            {
                damagePlayer(player,damage);
            }
            else
            {
                move(FIELD,up,down,left,right);
            }
            if(amountSpecialAttack > 0)
            {
                specialAttack(FIELD);
            }
        }
       else deray--;
       if(flagSpecialAttack)
       {
           startSpecialAttack(FIELD,player);
       }
    }

    private void startSpecialAttack(Block[][] FIELD,Player player)
    {
        if(distance > 0)
        {
            switch (directionSpecialAttack)
            {
                case 1: {outgoingYpos--;}break;
                case 2: {outgoingYpos++;}break;
                case 3: {outgoingXpos--;}break;
                case 4: {outgoingXpos++;}break;
            }
            if(FIELD[outgoingYpos][outgoingXpos].getObject() == ObjectField.PLAYER)
            {
                player.damage(damageSpecialAttack);
                flagSpecialAttack = false;
            }
            else
            {
                FIELD[outgoingYpos][outgoingXpos].setObject(ObjectField.SPEAR);
            }
            distance--;
        }
        else
        {
            flagSpecialAttack = false;
        }
    }
}
