package monster;

import varia.Block;
import varia.Player;

import java.io.Serializable;
import java.util.ArrayList;

public class Monsters extends Monster implements Serializable
{
    private ArrayList<Monster> monsters = new ArrayList<>();

    public void addComponent(Monster monster)
    {
        monsters.add(monster);
    }

    public void AImob(Block[][] field, Player player)
    {
        for(Monster item : monsters)
        {
            item.AImob(field,player);
        }
    }

    public boolean isEmpty()
    {
        return monsters.isEmpty();
    }

    public int damage(int d)
    {
        for(Monster item : monsters)
        {
            item.damage(d);
        }
        return 0;
    }

    public void removeAllMonsters()
    {
        monsters.removeAll(monsters);
    }

    public boolean findMonster(int y,int x,Player player)
    {
        for(Monster item : monsters)
        {
            if(item.findMonster(y,x,player))
            {
                int tempScore = item.damage(player.getDamageBomb());
                if(tempScore > 0)
                {
                    monsters.remove(item);
                    player.addScore(tempScore);
                    return true;
                }
                break;
            }
        }
        return false;
    }
}
