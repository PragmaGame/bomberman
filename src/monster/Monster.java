package monster;

import varia.Block;
import varia.ObjectField;
import varia.Player;

import java.io.Serializable;
import java.util.Random;

abstract class Monster implements Serializable
{
    int health;
    int shield;
    int damage;
    int yPosMonster;
    int xPosMonster;
    int ID;
    int scoreMonster;
    ObjectField objectMonster;

    abstract void AImob(Block[][] FIELD, Player player);
    abstract boolean findMonster(int y,int x,Player player);
    abstract int damage(int d);

    public int getyPosMonster() { return yPosMonster; }
    public int getxPosMonster() { return xPosMonster; }

    void damagePlayer(Player player,int currentDamage)
    {
        player.damage(currentDamage);
    }

    void move(Block[][] FIELD, ObjectField up, ObjectField down, ObjectField left, ObjectField right)
    {
        ObjectField[] opportunity = new ObjectField[]{up,down,left,right,ObjectField.NOTHING};
        boolean[] opportunityMoveUpDownLeftRightStay = new boolean[5];
        for (int i = 0; i < opportunity.length ; i++)
        {
            if(opportunity[i] == ObjectField.NOTHING || opportunity[i] == ObjectField.SHIELD || opportunity[i] == ObjectField.HELTH)
            {
                opportunityMoveUpDownLeftRightStay[i] = true;
            }
            else opportunityMoveUpDownLeftRightStay[i] = false;
        }
        FIELD[yPosMonster][xPosMonster].setObject(ObjectField.NOTHING);
        switch (randomMove(opportunityMoveUpDownLeftRightStay))
        {
            case 0 : yPosMonster--;break;
            case 1 : yPosMonster++;break;
            case 2 : xPosMonster--;break;
            case 3 : xPosMonster++;break;
            case 4 : break;
        }
        FIELD[yPosMonster][xPosMonster].setObject(objectMonster);
    }

    int randomMove(boolean[] opportunity)
    {
        for (;;)
        {
            int random = random(5);
            for (int i = 0; i < opportunity.length; i++)
            {
                if (opportunity[i] == true && i == random)
                {
                    return random;
                }
            }
        }
    }

    int random(int ran)
    {
        Random random = new Random();
        return random.nextInt(ran);
    }
}
