package monster;

import varia.Block;
import varia.ObjectField;
import varia.Player;

public class MonsterGhost extends Monster
{
    MonsterGhost(int y,int x,int id)
    {
        ID = id;
        yPosMonster = y;
        xPosMonster = x;
        health = 100;
        shield = 0;
        damage = 50;
        scoreMonster = 100;
        objectMonster = ObjectField.ENEMY_GHOST;
    }

    public void specialAttack(Player player)
    {
        int damageSpecialAttack = 100;
        if(random(5) < 1)
        {
            damagePlayer(player,damageSpecialAttack);
        }
    }


    int damage(int d)
    {
        if(random(10) > 3)
        {
            health -= d;
        }
        if(health <= 0 )
            return scoreMonster;
        return 0;
    }

    public boolean findMonster(int y,int x,Player player)
    {
        return y == yPosMonster && x == xPosMonster;
    }

    public void AImob(Block[][] FIELD, Player player)
    {
        ObjectField up = FIELD[yPosMonster - 1][xPosMonster].getObject();
        ObjectField down = FIELD[yPosMonster + 1][xPosMonster].getObject();
        ObjectField left = FIELD[yPosMonster][xPosMonster - 1].getObject();
        ObjectField right = FIELD[yPosMonster][xPosMonster + 1].getObject();
        if(up == ObjectField.PLAYER || down == ObjectField.PLAYER || left == ObjectField.PLAYER || right == ObjectField.PLAYER)
        {
            specialAttack(player);
            damagePlayer(player,damage);
        }
        else
        {
            move(FIELD,up,down,left,right);
        }
    }
}
