package monster;

public class FactoryMonster
{
    public Monster create(MonsterTypes type,int y,int x,int id)
    {
        switch (type)
        {
            case GHOST: return new MonsterGhost(y,x,id);
            case KNIGHT: return new MonsterKnight(y,x,id);
            case SPIDER: return  new MonsterSpider(y,x,id);
            case SPIDERTOXIC: return new MonsterSpiderToxic(y,x,id);
            case KNIGHTSHIELD: return new MonsterKnightShield(y,x,id);
            default: throw new IllegalArgumentException("There is no such monster : " + type);
        }
    }
}
