package monster;

import varia.Block;
import varia.Player;

import java.io.Serializable;

public class AI implements Serializable
{
    private Monsters monsters = new Monsters();
    private int deray = 0;

    public AI()
    {
        FactoryMonster factory = new FactoryMonster();
        Monster monster1 = factory.create(MonsterTypes.GHOST,1,19,1);
        Monster monster2 = factory.create(MonsterTypes.GHOST,9,16,2);
        Monster monster3 = factory.create(MonsterTypes.GHOST,13,26,3);
        Monster monster4 = factory.create(MonsterTypes.KNIGHT,1,27,4);
        Monster monster5 = factory.create(MonsterTypes.KNIGHTSHIELD,2,1,5);
        Monster monster6 = factory.create(MonsterTypes.SPIDER,13,2,6);

        // Отдельная композиция для одного подкласса монстра (Призраки)
        Monsters monstersGhost = new Monsters();
        monstersGhost.addComponent(monster1);
        monstersGhost.addComponent(monster2);
        monstersGhost.addComponent(monster3);

        monsters.addComponent(monstersGhost);
        monsters.addComponent(monster4);
        monsters.addComponent(monster5);
        monsters.addComponent(monster6);
    }

    public void run(Block[][] FIELD, Player player)
    {
        int finalDeray = 4;
        if(deray == finalDeray)
        {
            monsters.AImob(FIELD,player);
            deray = 0;
        }
        else
        {
            deray++;
        }
    }

    public boolean Victory()
    {
        return monsters.isEmpty();
    }

    public void damageMonster(int y, int x, Player player)
    {
        monsters.findMonster(y,x,player);
    }

    public void removeAllMonsters()
    {
        monsters.removeAllMonsters();
    }
}
