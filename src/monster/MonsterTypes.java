package monster;

public enum MonsterTypes
{
    GHOST,
    KNIGHT,
    KNIGHTSHIELD,
    SPIDER,
    SPIDERTOXIC,
    BOSS
}
