package monster;

import varia.Block;
import varia.ObjectField;
import varia.Player;

public class MonsterSpider extends Monster
{
    MonsterSpider(int y,int x,int id)
    {
        ID = id;
        yPosMonster = y;
        xPosMonster = x;
        health = 100;
        shield = 0;
        damage = 30;
        scoreMonster = 100;
        objectMonster = ObjectField.ENEMY_SPIDER;
    }

    int damage(int d)
    {
        health -= d;
        if(health <= 0 )
            return scoreMonster;
        return 0;
    }

    public boolean findMonster(int y,int x,Player player)
    {
        return y == yPosMonster && x == xPosMonster;
    }

    @Override
    void move(Block[][] FIELD, ObjectField up, ObjectField down, ObjectField left, ObjectField right)
    {
        ObjectField[] opportunity = new ObjectField[]{up,down,left,right,ObjectField.NOTHING};
        boolean[] opportunityMoveUpDownLeftRightStay = new boolean[5];
        for (int i = 0; i < opportunity.length ; i++)
        {
            if(opportunity[i] == ObjectField.NOTHING || opportunity[i] == ObjectField.SHIELD || opportunity[i] == ObjectField.HELTH || opportunity[i] == ObjectField.WEB)
            {
                opportunityMoveUpDownLeftRightStay[i] = true;
            }
            else opportunityMoveUpDownLeftRightStay[i] = false;
        }
        FIELD[yPosMonster][xPosMonster].setObject(ObjectField.WEB);
        switch (randomMove(opportunityMoveUpDownLeftRightStay))
        {
            case 0 : yPosMonster--;break;
            case 1 : yPosMonster++;break;
            case 2 : xPosMonster--;break;
            case 3 : xPosMonster++;break;
            case 4 : break;
        }
        FIELD[yPosMonster][xPosMonster].setObject(objectMonster);
    }

    public void AImob(Block[][] FIELD, Player player)
    {
        ObjectField up = FIELD[yPosMonster - 1][xPosMonster].getObject();
        ObjectField down = FIELD[yPosMonster + 1][xPosMonster].getObject();
        ObjectField left = FIELD[yPosMonster][xPosMonster - 1].getObject();
        ObjectField right = FIELD[yPosMonster][xPosMonster + 1].getObject();
        if(up == ObjectField.PLAYER || down == ObjectField.PLAYER || left == ObjectField.PLAYER || right == ObjectField.PLAYER)
        {
            damagePlayer(player,damage);
        }
        else
        {
            move(FIELD,up,down,left,right);
        }
    }
}
