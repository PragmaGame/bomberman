package data;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class RecordScore
{
    public int gameRecordScore ;
    public String userName ;
    public String dateRecord ;

    RecordScore(int gameRecordScore,String userName,String dateRecord)
    {
        this.gameRecordScore = gameRecordScore;
        this.userName = userName;
        this.dateRecord = dateRecord;
    }

    static class ScoreComparator implements Comparator<RecordScore>
    {
        @Override
        public int compare(RecordScore a, RecordScore b)
        {
            if (a.gameRecordScore < b.gameRecordScore)
                return 1;
            else
                return -1;
        }
    }

    private static String convertDataToString(Date date)
    {
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd.MM.yyyy");
        String str = formatForDateNow.format(date);
        return str;
    }

    static public void saveRecord(ArrayList<RecordScore> listRecordScore, int gameScore, String str)
    {
        if (listRecordScore.size() < 10)
        {
            Date dateRecord = new Date();
            RecordScore Scorex = new RecordScore(gameScore,str,convertDataToString(dateRecord));
            listRecordScore.add(Scorex);
        }
        else
        {
            if (listRecordScore.get(9).gameRecordScore < gameScore)
            {
                Date dateRecord = new Date();
                RecordScore Scorex = new RecordScore(gameScore,str,convertDataToString(dateRecord));
                listRecordScore.remove(9);
                listRecordScore.add(Scorex);
            }
        }
        // Сортируем коллекцию
        Collections.sort(listRecordScore, new RecordScore.ScoreComparator());
    }

    static public int outputRecordScore(ArrayList<RecordScore> listRecordScore, int gameScore)
    {
        int xRecordScore = 0;
        if(listRecordScore.isEmpty() == true)
        {
            xRecordScore = 0;
        }
        else
        {
            for (int i = listRecordScore.size(); i > 0;i-- )
            {
                if (gameScore < listRecordScore.get(i - 1).gameRecordScore)
                {
                    xRecordScore = listRecordScore.get(i - 1).gameRecordScore;
                    return xRecordScore;
                }
            }
            xRecordScore = gameScore;
        }
        return xRecordScore;
    }
}
