package data;

import varia.GameField;

import java.io.*;

public class Serialization
{
    //windows
    private static final String filePathGame =  "D:\\BSUIR\\Practice\\JAVA\\Bomberman\\Game.dat";
    // linux
    //private static final String filePathGame =  "/home/pragma/Project/JAVA/Bomberman/Game.dat";

    public static SerializationFile Load()
    {
        File myFile = new File(filePathGame);
        if(myFile.exists())
        {
            System.out.println("Файл сереализации игры существует");
            SerializationFile file = DeserializeGame();
            myFile.delete();
            return file;
        }
        else
        {
            System.out.println("Файл сереализации игры не существует");
            return null;
        }
    }

    public static void SerializableGame(SerializationFile file)
    {
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePathGame)))
        {
            oos.writeObject(file);
            System.out.println("Сереализация Игры : OK");
        }
        catch(Exception ex)
        {
            System.out.println("Серелизация Игры : Error");
            System.out.println(ex.getMessage());
        }
    }

    public static SerializationFile DeserializeGame()
    {
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filePathGame)))
        {
            SerializationFile file = (SerializationFile)ois.readObject();
            System.out.println("Десереализация Игры : OK");
            return file;
        }
        catch(Exception ex)
        {
            System.out.println("Десерелизация Игры : Error");
            System.out.println(ex.getMessage());
            return null;
        }
    }
}
