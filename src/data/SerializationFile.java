package data;

import monster.AI;
import monster.Monsters;
import varia.Block;
import varia.Player;

import java.io.Serializable;

public class SerializationFile implements Serializable
{
    public Player _player;
    public Block[][] _block;
    public boolean[] _bool;
    public AI _ai;


    public void SaveStateGame(Player player,Block[][] block, boolean[] bool,AI ai)
    {
        _player = player;
        _block = block;
        _bool = bool;
        _ai = ai;
    }
}
