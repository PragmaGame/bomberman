package data;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;

public class DataBase
{
    Connection co;

    private void open()
    {
        try
        {
            Class.forName("org.sqlite.JDBC");
            // windows
            co = DriverManager.getConnection("jdbc:sqlite:D:\\BSUIR\\Practice\\JAVA\\Bomberman\\sqlite\\RecordScore.db");
            // Linux
            // co = DriverManager.getConnection("jdbc:sqlite:/home/pragma/project/JAVA/Bomberman/sqlite/RecordScore.db");
            System.out.println("Connection sqlite true");
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println("Conection sqlite error");
        }
    }

    private void close()
    {
        try
        {
            co.close();
            System.out.println("Close sqlite true");
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println("Close sqlite error");
        }
    }

    public ArrayList<RecordScore> loadingRecord()
    {
        try
        {
            open();
            ArrayList<RecordScore> listRecordScore = new ArrayList<>();

            Statement statement = co.createStatement();
            String query =
                    "SELECT score, name, data " +
                    "FROM RecordScore";
            ResultSet rs = statement.executeQuery(query);
            while(rs.next())
            {
                    listRecordScore.add(new RecordScore(rs.getInt("score"),rs.getString("name"),rs.getString("data")));
            }
            rs.close();
            statement.close();
            close();
            return listRecordScore;
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public void saveRecord(ArrayList<RecordScore> listRecordScore)
    {
        try
        {
            open();
            String queryDelete =
                    "DELETE FROM RecordScore";
            Statement statement = co.createStatement();
            statement.executeUpdate(queryDelete);

            for(int i = 0 ; i < listRecordScore.size();i++)
            {
                String query =
                        "INSERT INTO RecordScore " +
                        "VALUES ('" + listRecordScore.get(i).gameRecordScore + "','" + listRecordScore.get(i).userName + "', '" + listRecordScore.get(i).dateRecord + "')";
                statement.executeUpdate(query);
            }
            statement.close();
            close();
            System.out.println("sqlite save record true");
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println("sqlite error insertRecord");
        }
    }

    public void insert(int score, String name, String data)
    {
        try
        {
            String query =
                    "INSERT INTO RecordScore " +
                    "VALUES ('" + score + "','" + name + "', '" + data + "')";
            Statement statement = co.createStatement();
            statement.executeUpdate(query);
            statement.close();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println("sqlite error insert");
        }
    }
}
