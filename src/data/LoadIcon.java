package data;

import javax.swing.*;
import java.awt.*;

public class LoadIcon
{
    public static Image fence;
    public static Image player;
    public static Image background;
    public static Image label;
    public static Image health;
    public static Image shield;
    public static Image bomb;
    public static Image knight;
    public static Image ghost;
    public static Image fenceironclad;
    public static Image explosion;
    public static Image HealthStatistics;
    public static Image ShieldStatistics;
    public static Image BombStatistics;
    public static Image ExplosionStatistics;
    public static Image ScoreStatistics;
    public static Image web;
    public static Image spider;
    public static Image spidertoxic;
    public static Image knightshield;
    public static Image menuBackground;
    public static Image bufBomb;
    public static Image bufBombRadius;
    public static Image bufBombDamage;
    public static Image Boss;
    public static Image Spear;

    public static void LoadLabel()
    {
        ImageIcon iil = new ImageIcon("Icon/Label.png");
        label = iil.getImage();
        ImageIcon iib = new ImageIcon("Icon/menuBackground.png");
        menuBackground = iib.getImage();

    }
    public static void LoadImages()
    {
        ImageIcon iiexp = new ImageIcon("Icon/ExplosionV2.png");
        explosion = iiexp.getImage();
        ImageIcon iif = new ImageIcon("Icon/FenceV41.png");
        fence = iif.getImage();
        ImageIcon iip = new ImageIcon("Icon/Bomberman.png");
        player = iip.getImage();
        ImageIcon iib = new ImageIcon("Icon/BackGround.png");
        background = iib.getImage();
        ImageIcon iih = new ImageIcon("Icon/Health.png");
        health = iih.getImage();
        ImageIcon iifi = new ImageIcon("Icon/FenceIroncladV41.png");
        fenceironclad = iifi.getImage();
        ImageIcon iis = new ImageIcon("Icon/shieldV4.png");
        shield = iis.getImage();
        ImageIcon iibo = new ImageIcon("Icon/Bomb.png");
        bomb = iibo.getImage();
        ImageIcon iisk = new ImageIcon("Icon/KnightV3.png");
        knight = iisk.getImage();
        ImageIcon iigh = new ImageIcon("Icon/ghost.png");
        ghost = iigh.getImage();
        ImageIcon iiht = new ImageIcon("Icon/HealthStatistics.png");
        HealthStatistics = iiht.getImage();
        ImageIcon iiss = new ImageIcon("Icon/ShieldStatistics.png");
        ShieldStatistics = iiss.getImage();
        ImageIcon iibs = new ImageIcon("Icon/BombStatistics.png");
        BombStatistics = iibs.getImage();
        ImageIcon iies = new ImageIcon("Icon/ExplosionStatistics.png");
        ExplosionStatistics = iies.getImage();
        ImageIcon iiscore = new ImageIcon("Icon/ScoreStatistics.png");
        ScoreStatistics = iiscore.getImage();
        ImageIcon sn = new ImageIcon("Icon/SpiderN.png");
        spider = sn.getImage();
        ImageIcon st = new ImageIcon("Icon/SpiderToxic.png");
        spidertoxic = st.getImage();
        ImageIcon w = new ImageIcon("Icon/web.png");
        web = w.getImage();
        ImageIcon ks = new ImageIcon("Icon/KnightShield.png");
        knightshield = ks.getImage();
        ImageIcon bufB = new ImageIcon("Icon/BombAdd.png");
        bufBomb = bufB.getImage();
        ImageIcon bufbombDamage = new ImageIcon("Icon/AddDamageBomb.png");
        bufBombDamage = bufbombDamage.getImage();
        ImageIcon bufbombra = new ImageIcon("Icon/AddRadiusBomb.png");
        bufBombRadius = bufbombra.getImage();
        ImageIcon bos = new ImageIcon("Icon/Boss.png");
        Boss = bos.getImage();
        ImageIcon spe = new ImageIcon("Icon/spear.png");
        Spear = spe.getImage();
    }

}
