import data.LoadIcon;
import data.Serialization;
import data.SerializationFile;
import varia.GameField;

import javax.swing.*;

public class Window extends JFrame
{
    public Window()
    {
        setTitle("BOMBERMAN");
        LoadIcon.LoadLabel();
        setIconImage(LoadIcon.label);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        // 1856 960
        setSize(1872,1050);
        setLocation(0,0);
        setVisible(true);
    }

    public static void main(String[] args)
    {
        Window window = new Window();
        SerializationFile file = Serialization.Load();
        GameField field = new GameField(file);
        window.add(field);
        window.setVisible(true);
    }

}